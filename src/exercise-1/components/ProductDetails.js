import React from 'react'

class ProductDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data:
        [
          {
            "id": 1,
            "name": "Bicycle",
            "price": 30,
            "quantity": 15,
            "desc": "Bicycle is Good"
          },
          {
            "id": 2,
            "name": "TV",
            "price": 40,
            "quantity": 16,
            "desc": "TV is good"
          },
          {
            "id": 3,
            "name": "Pencil",
            "price": 50,
            "quantity": 17,
            "desc": "Pencil is good"
          }
        ]

    };
  }

  render() {
    let res;
    this.state.data.forEach((value) => {
      if (Number.parseInt(value.id) === Number.parseInt(this.props.match.params.id)) {
        res = value;
      }
    });
    if (res === undefined) {
      return (
        <p>not fount</p>
      )
    }
    return (
      <div>
        <p>Product Details</p>
        Name:{res.name}<br/>
        id:{res.id}<br/>
        Price:{res.price}<br/>
        Quantity:{res.quantity}<br/>
        Desc:{res.desc}<br/>
        URL:{this.props.match.url}<br/>
      </div>
    )
  }
}
export default ProductDetails
