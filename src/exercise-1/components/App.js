import React, {Component} from 'react';
import '../styles/App.css';
import { BrowserRouter as Router, NavLink, Route } from 'react-router-dom';
import Home from "./Home";
import AboutUs from "./AboutUs";
import MyProfile from "./MyProfile";
import { Switch } from "react-router";
import '../styles/App.css';
import Products from "./Products";

class App extends Component {
  render() {
    return (
      <Router>
        <nav>
          <ul>
            <li>
              <NavLink exact to='/' onClick={this.clickUrl}><span>Home</span></NavLink>
            </li>
            <li>
              <NavLink exact to='/products' onClick={this.clickUrl}><span>Products</span></NavLink>
            </li>
            <li>
              <NavLink to="/about" onClick={this.clickUrl}><span>About</span></NavLink>
            </li>
            <li>
              <NavLink to="/my-porfile"
                       onClick={this.clickUrl}><span>MyProfile</span></NavLink>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/(products|goods)" component={Products}/>
          <Route path="/my-porfile" component={MyProfile}/>
          <Route path="/about" component={AboutUs}/>
        </Switch>
      </Router>

    );
  }
}

export default App;
